<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>
<body>
    <form id="login-form" method="POST" action="{{ url('/login/doLogin') }}">
        @csrf

        <label for="username">
            <input type="text" name="username" id="username" placeholder="Username" autocomplete="off" tabindex="0">
        </label>

        <p>
        <label for="password">
            <input type="password" name="password" id="password" placeholder="Password" autocomplete="off" tabindex="0">
        </label>
        </p>

        <p>
        <button type="submit" id="loginButton" tabindex="0">Login</button>
        </p>
    </form>
</body>
</html>
