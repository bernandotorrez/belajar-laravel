<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testUserCanLoginAndGoToDashboard()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('username', 'bernandotorrez')
                    ->type('password', 'B3rnando')
                    ->click('#loginButton')
                    ->assertPathIs('/belajar/dashboard');
        });
    }
}
