<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index() {
        return view('login');
    }

    public function doLogin(Request $request) {
        $username = $request->post('username');
        $password = $request->post('password');

        if($username == 'bernandotorrez' && $password == 'B3rnando') {
            return redirect('/dashboard');
        } else {
            return redirect('/login');
        }
    }
}
